# -*- coding: utf-8 -*-
"""
Created on Mon Dec 28 15:41:56 2020

@author: chris

Extract samples from a dataset. stratified_sample keep the same number of samples.
biased_sample prioritize the samples based on some criterions.
"""

import pandas    as pd
import numpy     as np


class BBS():
    def __init__(self, CSV):
        self.df = pd.read_csv(
            CSV,
            sep='|'
            )
        
    def stratified_sample(self, n_samples, *col):
        n = min(n_samples, self.df[list(col)].value_counts().min())
        df_ = self.df.groupby(list(col)).apply(lambda x: x.sample(n))
        df_.index = df_.index.droplevel(0)
        return df_
    
    
    def biased_sample(self, N, *col):
        return self.df.groupby(list(col), group_keys=False)
                      .apply(lambda x: x.sample(int(np.rint(N*len(x)/len(self.df)))))
                      .sample(frac=1).reset_index(drop=True)
    
